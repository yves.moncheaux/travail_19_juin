# Les permissions relatives aux fichiers dans GNU/Linux

# Éléments de la fiche

## Sujet abordé
Les permissions relatives aux fichiers dans GNU/Linux, que sont-elles ? Comment les consulter ou les modifier ?

## Objectifs

+ Systèmes d'exploitation/Utiliser les commandes de bases en ligne de commande.
+ Systèmes d'exploitation/Gérer les droits et permissions d'accès aux fichiers.
+ Représentation des données/Ecriture d'un entier positif en base 8.
+ Représentation des données/Représentation binaire d'un entier relatif.
+ Représentation des données/Valeurs et opérateurs booléens.

## Pré-requis

* écriture binaire d'un nombre entier positif;
* navigation dans un système Unix (commande `cd`), création d'un dossier ou d'un fichier (commandes `mkdir` et `touch`);
* propriétaire d'un fichier, groupes;
* écriture binaire d'un nombre entier positif ;
* navigation dans un système Unix (commande `cd`), création d'un dossier ou d'un fichier (commandes `mkdir` et `touch`) ;
* connaissance des notions d'utilisateurs et de groupes.

## Préparation

Avoir accès à un ordinateur avec GNU/Linux ou à une machine virtuelle (Virtualbox).

## Éléments de cours

En introduction, on rappellera l'un des grands principes de conception
des systèmes Unix, la séparation des règles (policy) et des mécanismes qui les mettent en oeuvre.

### 1. Les règles

Les droits et permissions d'un utilisateur relativement à un fichier sont entièrement contenus dans un nombre - le mode du fichier.

Trois opérations sont permises - ou non : la lecture (r), l'écriture (w) et l'exécution (x).

Dans le cas particulier des répertoires, l'exécution correspond à la lecture de la liste des fichiers contenus dans le répertoire.

### 2. Codage des règles : le mode

Le mode est un nombre composé de trois groupes de trois bits, et est donc souvent représenté par un nombre à trois chiffres en octal (base 8).

Description des trois bits : lecture, écriture, exécution. Chiffre octal correspondant.

Les trois chiffres en octal : les droits de l'utilisateur (user), de son groupe (group), de tous les autres (others). 


### 3. Les mécanismes

Tests sur divers fichiers et répertoires donnés aux élèves.

La fonction `chmod`, ses deux syntaxes, utilisation.

Pour les plus rapides, on peut envisager un TP bis sur la fonction `umask`, en utilisation en conjonction avec les commandes `touch` et `mkdir`.

## Séance pratique

* sur papier : extraire les informations d'une liste de séquences de la forme rwxr--rw-
* sur ordinateur : 
    * extraire un dossier compressé fourni par le professeur et donner les permissions des fichiers et répertoires qui y sont contenus ;
    * ou plus simplement, récupérer les permissions de fichiers du système spécifiés par le professeur.

* sur papier (ou dans un exerciseur en ligne), compléter un tableau de conversion entre la forme symbolique et la forme numérique (pour la commande `chmod`) ;
* application sur ordinateur :
    * créer un fichier avec `touch` ; observer ses droits avec `ls -l`, les modifier de différentes façons suivant des consignes données ;
    * mêmes opérations (création, observation, modification) avec un répertoire;
    * tests d'accès avec un autre utilisateur créé préalablement par l'élève;
    * observation des droits de certains fichiers du système.
    * tests d'accès avec un autre utilisateur créé préalablement par l'élève.

* pour terminer, on peut éventuellement inviter les élèves à écrire des fonctions en Python qui convertissent :
    * de la forme symbolique vers la forme octale (exemple rwxr--rw- -> 746) ;
    * de la forme octale vers la forme symbolique (exemple 746 -> rwxr--rw-) ;



# TP bis sur `umask` pour les plus rapides :

* explications sur l'utilité de umask ;
* sur ordinateur : 
    * création d'un dossier, déplacement dans celui-ci puis création de fichiers et de répertoires (avec `touch` et `mkdir`) et observation des droits par défaut ;
    * utilisation de `umask` dans celui-ci, création de fichiers et de répertoires et observation des droits ainsi obtenus ;
    * proposition de quelques commandes (exemple : `umask 022`) et mise en correspondance avec le code octal correspondant aux droits des fichiers ou des répertoires...

* explications, à l'aide d'un exemple, de la conversion du code octal utilisé par `umask` vers celui utilisable par `chmod` ;
* sur papier (ou dans un exerciseur en ligne) :
    * exercice de conversion d'un nombre donné en format binaire vers la forme octale et réciproquement ;
    * exercice sur la conversion de la base 10 vers la base 2 puis vers la base 8 et réciproquement ;
    * exercice sur l'opérateur NOT en binaire ;
    * exercice sur l'opérateur AND en binaire ;
    * exercice de conversion du code octal d'`umask` vers le code correspondant à celui de `chmod` ;
    * quelle commande `umask` faut-il utiliser pour que les fichiers et les dossiers créés aient tel ou tel droit ?
* sur ordinateur : 
    * création d'un dossier, déplacement dans celui-ci puis création de fichiers et de répertoires et observation des droits par défaut ;
    * utilisation de `umask` dans celui-ci, création de fichiers et de répertoires et observation des droits ainsi définis ;
    * utilisation de `chmod` éventuelle pour rectifier les droits de certains fichiers (par exemple un fichier contenant des mots de passe)
* pour les plus rapides, écrire des fonctions en Python qui convertissent
    * du nombre utilisé par`umask` vers les modes correspondants pour les fichiers ou les dossiers (exemple 027 -> 640 et 750)
    * création de fichiers et de dossiers ayant certains droits, utilisation de `chmod` éventuelles pour rectifier les droits de certains (par exemple un fichier contenant des mots de passe) ;
    * pour finir (pour les champions...), écrire une fonction qui convertit le nombre utilisé par `umask` vers les modes correspondants pour les fichiers ou les dossiers (exemple 027 -> 640 et 750).

## QCM E3C

QCM 1.1

La commande permettant de lister tous les fichiers du répertoire courant est :

Réponses

A chmod

B ls

C mkdir

D cd

---


QCM 1.2

Les permissions sur un fichier : lecture pour tous, écriture et exécution pour tous les membres du groupe (utilisateur courant inclus) sont codifiées par :

Réponses

A drwxrwxr--

B -rwxr--rwx

C dr--rwxrwx

D -rwxrwxr--

---

QCM 1.3

Le chiffre octal correspondant à des permissions en écriture et en exécution est :

Réponses

A 6

B 4

C 3

D 2


---

QCM 1.4

La commande `ls -l` a affiché, entre autres, le résultat 

drwxrwxr-x 2 user user  4096 juin   6 14:55 extens

Réponses

A l'utilisateur peut entrer dans le dossier *extens*

B l'utilisateur peut exécuter le fichier *extens*

C l'utilisateur ne peut pas entrer dans le dossier *extens*

D l'utilisateur ne peut pas exécuter le fichier *extens*


---

QCM 1.5

La commande `chmod 412 mon_fichier` donne au groupe de l'utilisateur un accès au fichier *mon_fichier* en :

Réponses

A lecture et écriture

B exécution seule

C lecture et exécution

D lecture seule

---

QCM 1.6

La commande permettant de donner au groupe une permission en écriture sur le fichier *mon_fichier* est :

Réponses

A chmod x+w mon_fichier

B chmod x+g mon_fichier

C chmod g+x mon_fichier

D chmod g+w mon_fichier
